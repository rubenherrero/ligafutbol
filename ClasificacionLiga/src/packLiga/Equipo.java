package packLiga;

public class Equipo {

	private String nombre;
	private int puntos;
	
	public Equipo( String nombre )
	{
		this.nombre = nombre;
		this.puntos = 0;
	}
	
	public int getPuntos()
	{
		return puntos;
	}
	
	public String getNombre()
	{
		return nombre;
	}
	
	public void incPuntos( int puntosDeMas )
	{
		puntos += puntosDeMas;
	}
	
	public static void main ( String args[] )
	{
		Equipo e = new Equipo("Athletic");
		
		e.incPuntos(1);
		
		e.incPuntos(3);
		
		System.out.println( e.getNombre() );
		System.out.println( e.getPuntos() );
	}
}

