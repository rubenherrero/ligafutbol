package packLiga;

public class Clasificacion {

	private Equipo equipo[]; // array de equipos

	private void creaArrayEquipos() {
		int jornadasJugadas = Liga.getJornadas();

		if (jornadasJugadas > 0) {
			// creas el array para almacenar 20 equipos
			equipo = new Equipo[20];

			// crear los 10 equipos
			for (int i = 0; i < 10; i++) {
				String nom = Liga.getEquipo1(0, i);

				equipo[i] = new Equipo(nom);
			}

			int j = 10;
			for (int i = 0; i < 10; i++) {
				String nom = Liga.getEquipo2(0, i);

				equipo[j] = new Equipo(nom);
				j++;
			}

		}
	}

	private void visualizaEquipos() {
		for (int i = 0; i < 20; i++) {
			System.out.println(equipo[i].getNombre() + " Puntos : " + equipo[i].getPuntos());
		}

	}

	private Equipo buscaEnArray(String nomEquipo) {
		int i = 0;
		boolean encontrado = false;
		while (i < 20 && encontrado == false) {
			if (equipo[i].getNombre().equals(nomEquipo)) {
				encontrado = true;
			} else {
				i++;
			}
		}

		if (encontrado) {
			return equipo[i];
		} else {
			return null;
		}
	}

	private void calcPuntos() {
		int jornadasJugadas = Liga.getJornadas();

		for (int j = 0; j < jornadasJugadas; j++) {
			for (int p = 0; p < 10; p++) {
				// goles equipo local
				int goles1 = Liga.getGoles1(j, p);

				// goles equipo visitante
				int goles2 = Liga.getGoles2(j, p);

				String nombre1 = Liga.getEquipo1(j, p);
				Equipo equipo1 = buscaEnArray(nombre1);

				String nombre2 = Liga.getEquipo2(j, p);
				Equipo equipo2 = buscaEnArray(nombre2);

				// ganan o pierden
				if (goles1 != goles2) {
					// ganador local
					if (goles1 > goles2) {

						equipo1.incPuntos(3);

					} else {

						equipo2.incPuntos(3);
					}
				}
				// empatan
				else {

					equipo1.incPuntos(1);

					equipo2.incPuntos(1);
				}
			}
		}
	}

	private void ordenaEquiposPorPuntos() {
		Equipo temp;
		// METODO DE ORDENACION BURBUJA
		for (int i = 0; i < 19; i++) {
			for (int j = i + 1; j < 20; j++) {
				if (equipo[i].getPuntos() < equipo[j].getPuntos()) {
					temp = equipo[i];
					equipo[i] = equipo[j];
					equipo[j] = temp;
				}
			}
		}

	}

	public static void main(String[] args) {
		Clasificacion clasif = new Clasificacion();
		clasif.creaArrayEquipos();
		clasif.calcPuntos();
		System.out.println("Equipos:");
		clasif.visualizaEquipos();
		clasif.ordenaEquiposPorPuntos();
		System.out.println("Clasificación:");
		clasif.visualizaEquipos();
	}

}

