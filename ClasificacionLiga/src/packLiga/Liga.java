package packLiga;
/** 
 * Clase principal de liga de fútbol. Práctica 0
 * Programación II - IEIAII
 */
public class Liga {

	private static String equipo1[][] = new String[38][10];  // Equipo 1 de cada partido [jornada][partido] (jornada de 0 a 37, partido de 0 a 9)
	private static String equipo2[][] = new String[38][10];  // Equipo 2 de cada partido [jornada][partido] (jornada de 0 a 37, partido de 0 a 9)
	private static int goles1[][] = new int[38][10];  // Goles de equipo 1 de cada partido [jornada][partido] (jornada de 0 a 37, partido de 0 a 9)
	private static int goles2[][] = new int[38][10];  // Goles de equipo 2 de cada partido [jornada][partido] (jornada de 0 a 37, partido de 0 a 9)
	
	static {  // Inicialización de resultados
		equipo1[0][0] = "Granada"; equipo2[0][0] = "Eibar"; goles1[0][0] = 1; goles2[0][0] = 3;
		equipo1[0][1] = "Betis"; equipo2[0][1] = "Villarreal"; goles1[0][1] = 1; goles2[0][1] = 1;
		equipo1[0][2] = "Levante"; equipo2[0][2] = "Celta"; goles1[0][2] = 1; goles2[0][2] = 2;
		equipo1[0][3] = "Sporting"; equipo2[0][3] = "R.Madrid"; goles1[0][3] = 0; goles2[0][3] = 0;
		equipo1[0][4] = "Athletic"; equipo2[0][4] = "Barcelona"; goles1[0][4] = 0; goles2[0][4] = 1;
		equipo1[0][5] = "Rayo"; equipo2[0][5] = "Valencia"; goles1[0][5] = 0; goles2[0][5] = 0;
		equipo1[0][6] = "Atlético"; equipo2[0][6] = "LasPalmas"; goles1[0][6] = 1; goles2[0][6] = 0;
		equipo1[0][7] = "Espanyol"; equipo2[0][7] = "Getafe"; goles1[0][7] = 1; goles2[0][7] = 0;
		equipo1[0][8] = "Deportivo"; equipo2[0][8] = "R.Sociedad"; goles1[0][8] = 0; goles2[0][8] = 0;
		equipo1[0][9] = "Málaga"; equipo2[0][9] = "Sevilla"; goles1[0][9] = 0; goles2[0][9] = 0;
		
		equipo1[1][0] = "Valencia"; equipo2[1][0] = "Deportivo"; goles1[1][0] = 1; goles2[1][0] = 1;
		equipo1[1][1] = "Sevilla"; equipo2[1][1] = "Atlético"; goles1[1][1] = 0; goles2[1][1] = 3;
		equipo1[1][2] = "Getafe"; equipo2[1][2] = "Granada"; goles1[1][2] = 1; goles2[1][2] = 2;
		equipo1[1][3] = "LasPalmas"; equipo2[1][3] = "Levante"; goles1[1][3] = 0; goles2[1][3] = 0;
		equipo1[1][4] = "Villarreal"; equipo2[1][4] = "Espanyol"; goles1[1][4] = 3; goles2[1][4] = 1;
		equipo1[1][5] = "R.Sociedad"; equipo2[1][5] = "Sporting"; goles1[1][5] = 0; goles2[1][5] = 0;
		equipo1[1][6] = "Barcelona"; equipo2[1][6] = "Málaga"; goles1[1][6] = 1; goles2[1][6] = 0;
		equipo1[1][7] = "Celta"; equipo2[1][7] = "Rayo"; goles1[1][7] = 3; goles2[1][7] = 0;
		equipo1[1][8] = "R.Madrid"; equipo2[1][8] = "Betis"; goles1[1][8] = 5; goles2[1][8] = 0;
		equipo1[1][9] = "Eibar"; equipo2[1][9] = "Athletic"; goles1[1][9] = 2; goles2[1][9] = 0;
		   
		equipo1[2][0] = "Rayo"; equipo2[2][0] = "Deportivo"; goles1[2][0] = 1; goles2[2][0] = 3;
		equipo1[2][1] = "Málaga"; equipo2[2][1] = "Eibar"; goles1[2][1] = 0; goles2[2][1] = 0;
		equipo1[2][2] = "Celta"; equipo2[2][2] = "LasPalmas"; goles1[2][2] = 3; goles2[2][2] = 3;
		equipo1[2][3] = "Athletic"; equipo2[2][3] = "Getafe"; goles1[2][3] = 3; goles2[2][3] = 1;
		equipo1[2][4] = "Granada"; equipo2[2][4] = "Villarreal"; goles1[2][4] = 1; goles2[2][4] = 3;
		equipo1[2][5] = "Betis"; equipo2[2][5] = "R.Sociedad"; goles1[2][5] = 1; goles2[2][5] = 0;
		equipo1[2][6] = "Atlético"; equipo2[2][6] = "Barcelona"; goles1[2][6] = 1; goles2[2][6] = 2;
		equipo1[2][7] = "Sporting"; equipo2[2][7] = "Valencia"; goles1[2][7] = 0; goles2[2][7] = 1;
		equipo1[2][8] = "Espanyol"; equipo2[2][8] = "R.Madrid"; goles1[2][8] = 0; goles2[2][8] = 6;
		equipo1[2][9] = "Levante"; equipo2[2][9] = "Sevilla"; goles1[2][9] = 1; goles2[2][9] = 1;
		   
		equipo1[3][0] = "Getafe"; equipo2[3][0] = "Málaga"; goles1[3][0] = 1; goles2[3][0] = 0;
		equipo1[3][1] = "R.Madrid"; equipo2[3][1] = "Granada"; goles1[3][1] = 1; goles2[3][1] = 0;
		equipo1[3][2] = "Valencia"; equipo2[3][2] = "Betis"; goles1[3][2] = 0; goles2[3][2] = 0;
		equipo1[3][3] = "Eibar"; equipo2[3][3] = "Atlético"; goles1[3][3] = 0; goles2[3][3] = 2;
		equipo1[3][4] = "R.Sociedad"; equipo2[3][4] = "Espanyol"; goles1[3][4] = 2; goles2[3][4] = 3;
		equipo1[3][5] = "Sevilla"; equipo2[3][5] = "Celta"; goles1[3][5] = 1; goles2[3][5] = 2;
		equipo1[3][6] = "Deportivo"; equipo2[3][6] = "Sporting"; goles1[3][6] = 2; goles2[3][6] = 3;
		equipo1[3][7] = "Villarreal"; equipo2[3][7] = "Athletic"; goles1[3][7] = 3; goles2[3][7] = 1;
		equipo1[3][8] = "Barcelona"; equipo2[3][8] = "Levante"; goles1[3][8] = 4; goles2[3][8] = 1;
		equipo1[3][9] = "LasPalmas"; equipo2[3][9] = "Rayo"; goles1[3][9] = 0; goles2[3][9] = 1;
		   
		equipo1[4][0] = "Betis"; equipo2[4][0] = "Deportivo"; goles1[4][0] = 1; goles2[4][0] = 2;
		equipo1[4][1] = "Málaga"; equipo2[4][1] = "Villarreal"; goles1[4][1] = 0; goles2[4][1] = 1;
		equipo1[4][2] = "LasPalmas"; equipo2[4][2] = "Sevilla"; goles1[4][2] = 2; goles2[4][2] = 0;
		equipo1[4][3] = "Athletic"; equipo2[4][3] = "R.Madrid"; goles1[4][3] = 1; goles2[4][3] = 2;
		equipo1[4][4] = "Rayo"; equipo2[4][4] = "Sporting"; goles1[4][4] = 2; goles2[4][4] = 1;
		equipo1[4][5] = "Levante"; equipo2[4][5] = "Eibar"; goles1[4][5] = 2; goles2[4][5] = 2;
		equipo1[4][6] = "Celta"; equipo2[4][6] = "Barcelona"; goles1[4][6] = 4; goles2[4][6] = 1;
		equipo1[4][7] = "Granada"; equipo2[4][7] = "R.Sociedad"; goles1[4][7] = 0; goles2[4][7] = 3;
		equipo1[4][8] = "Espanyol"; equipo2[4][8] = "Valencia"; goles1[4][8] = 1; goles2[4][8] = 0;
		equipo1[4][9] = "Atlético"; equipo2[4][9] = "Getafe"; goles1[4][9] = 2; goles2[4][9] = 0;
		   
		equipo1[5][0] = "Sporting"; equipo2[5][0] = "Betis"; goles1[5][0] = 1; goles2[5][0] = 2;
		equipo1[5][1] = "Deportivo"; equipo2[5][1] = "Espanyol"; goles1[5][1] = 3; goles2[5][1] = 0;
		equipo1[5][2] = "Getafe"; equipo2[5][2] = "Levante"; goles1[5][2] = 3; goles2[5][2] = 0;
		equipo1[5][3] = "R.Sociedad"; equipo2[5][3] = "Athletic"; goles1[5][3] = 0; goles2[5][3] = 0;
		equipo1[5][4] = "Valencia"; equipo2[5][4] = "Granada"; goles1[5][4] = 1; goles2[5][4] = 0;
		equipo1[5][5] = "Barcelona"; equipo2[5][5] = "LasPalmas"; goles1[5][5] = 2; goles2[5][5] = 1;
		equipo1[5][6] = "R.Madrid"; equipo2[5][6] = "Málaga"; goles1[5][6] = 0; goles2[5][6] = 0;
		equipo1[5][7] = "Villarreal"; equipo2[5][7] = "Atlético"; goles1[5][7] = 1; goles2[5][7] = 0;
		equipo1[5][8] = "Sevilla"; equipo2[5][8] = "Rayo"; goles1[5][8] = 3; goles2[5][8] = 2;
		equipo1[5][9] = "Eibar"; equipo2[5][9] = "Celta"; goles1[5][9] = 1; goles2[5][9] = 1;
		   
		equipo1[6][0] = "Celta"; equipo2[6][0] = "Getafe"; goles1[6][0] = 0; goles2[6][0] = 0;
		equipo1[6][1] = "Sevilla"; equipo2[6][1] = "Barcelona"; goles1[6][1] = 2; goles2[6][1] = 1;
		equipo1[6][2] = "Granada"; equipo2[6][2] = "Deportivo"; goles1[6][2] = 1; goles2[6][2] = 1;
		equipo1[6][3] = "Espanyol"; equipo2[6][3] = "Sporting"; goles1[6][3] = 1; goles2[6][3] = 2;
		equipo1[6][4] = "LasPalmas"; equipo2[6][4] = "Eibar"; goles1[6][4] = 0; goles2[6][4] = 2;
		equipo1[6][5] = "Málaga"; equipo2[6][5] = "R.Sociedad"; goles1[6][5] = 3; goles2[6][5] = 1;
		equipo1[6][6] = "Rayo"; equipo2[6][6] = "Betis"; goles1[6][6] = 0; goles2[6][6] = 2;
		equipo1[6][7] = "Athletic"; equipo2[6][7] = "Valencia"; goles1[6][7] = 3; goles2[6][7] = 1;
		equipo1[6][8] = "Levante"; equipo2[6][8] = "Villarreal"; goles1[6][8] = 1; goles2[6][8] = 0;
		equipo1[6][9] = "Atlético"; equipo2[6][9] = "R.Madrid"; goles1[6][9] = 1; goles2[6][9] = 1;
		   
		equipo1[7][0] = "R.Madrid"; equipo2[7][0] = "Levante"; goles1[7][0] = 3; goles2[7][0] = 0;
		equipo1[7][1] = "Eibar"; equipo2[7][1] = "Sevilla"; goles1[7][1] = 1; goles2[7][1] = 1;
		equipo1[7][2] = "Barcelona"; equipo2[7][2] = "Rayo"; goles1[7][2] = 5; goles2[7][2] = 2;
		equipo1[7][3] = "Valencia"; equipo2[7][3] = "Málaga"; goles1[7][3] = 3; goles2[7][3] = 0;
		equipo1[7][4] = "Betis"; equipo2[7][4] = "Espanyol"; goles1[7][4] = 1; goles2[7][4] = 3;
		equipo1[7][5] = "Villarreal"; equipo2[7][5] = "Celta"; goles1[7][5] = 1; goles2[7][5] = 2;
		equipo1[7][6] = "R.Sociedad"; equipo2[7][6] = "Atlético"; goles1[7][6] = 0; goles2[7][6] = 2;
		equipo1[7][7] = "Getafe"; equipo2[7][7] = "LasPalmas"; goles1[7][7] = 4; goles2[7][7] = 0;
		equipo1[7][8] = "Deportivo"; equipo2[7][8] = "Athletic"; goles1[7][8] = 2; goles2[7][8] = 2;
		equipo1[7][9] = "Sporting"; equipo2[7][9] = "Granada"; goles1[7][9] = 3; goles2[7][9] = 3;
		   
		equipo1[8][0] = "Athletic"; equipo2[8][0] = "Sporting"; goles1[8][0] = 3; goles2[8][0] = 0;
		equipo1[8][1] = "Atlético"; equipo2[8][1] = "Valencia"; goles1[8][1] = 2; goles2[8][1] = 1;
		equipo1[8][2] = "Barcelona"; equipo2[8][2] = "Eibar"; goles1[8][2] = 3; goles2[8][2] = 1;
		equipo1[8][3] = "LasPalmas"; equipo2[8][3] = "Villarreal"; goles1[8][3] = 0; goles2[8][3] = 0;
		equipo1[8][4] = "Levante"; equipo2[8][4] = "R.Sociedad"; goles1[8][4] = 0; goles2[8][4] = 4;
		equipo1[8][5] = "Málaga"; equipo2[8][5] = "Deportivo"; goles1[8][5] = 2; goles2[8][5] = 0;
		equipo1[8][6] = "Sevilla"; equipo2[8][6] = "Getafe"; goles1[8][6] = 5; goles2[8][6] = 0;
		equipo1[8][7] = "Granada"; equipo2[8][7] = "Betis"; goles1[8][7] = 1; goles2[8][7] = 1;
		equipo1[8][8] = "Celta"; equipo2[8][8] = "R.Madrid"; goles1[8][8] = 1; goles2[8][8] = 3;
		equipo1[8][9] = "Rayo"; equipo2[8][9] = "Espanyol"; goles1[8][9] = 3; goles2[8][9] = 0;
		   
		equipo1[9][0] = "Eibar"; equipo2[9][0] = "Rayo"; goles1[9][0] = 1; goles2[9][0] = 0;
		equipo1[9][1] = "Espanyol"; equipo2[9][1] = "Granada"; goles1[9][1] = 1; goles2[9][1] = 1;
		equipo1[9][2] = "Sporting"; equipo2[9][2] = "Málaga"; goles1[9][2] = 1; goles2[9][2] = 0;
		equipo1[9][3] = "Betis"; equipo2[9][3] = "Athletic"; goles1[9][3] = 1; goles2[9][3] = 3;
		equipo1[9][4] = "R.Sociedad"; equipo2[9][4] = "Celta"; goles1[9][4] = 2; goles2[9][4] = 3;
		equipo1[9][5] = "Getafe"; equipo2[9][5] = "Barcelona"; goles1[9][5] = 0; goles2[9][5] = 2;
		equipo1[9][6] = "Valencia"; equipo2[9][6] = "Levante"; goles1[9][6] = 3; goles2[9][6] = 0;
		equipo1[9][7] = "Villarreal"; equipo2[9][7] = "Sevilla"; goles1[9][7] = 2; goles2[9][7] = 1;
		equipo1[9][8] = "R.Madrid"; equipo2[9][8] = "LasPalmas"; goles1[9][8] = 3; goles2[9][8] = 1;
		equipo1[9][9] = "Deportivo"; equipo2[9][9] = "Atlético"; goles1[9][9] = 1; goles2[9][9] = 1;
		   
		equipo1[10][0] = "LasPalmas"; equipo2[10][0] = "R.Sociedad"; goles1[10][0] = 2; goles2[10][0] = 0;
		equipo1[10][1] = "Celta"; equipo2[10][1] = "Valencia"; goles1[10][1] = 1; goles2[10][1] = 5;
		equipo1[10][2] = "Levante"; equipo2[10][2] = "Deportivo"; goles1[10][2] = 1; goles2[10][2] = 1;
		equipo1[10][3] = "Rayo"; equipo2[10][3] = "Granada"; goles1[10][3] = 2; goles2[10][3] = 1;
		equipo1[10][4] = "Eibar"; equipo2[10][4] = "Getafe"; goles1[10][4] = 3; goles2[10][4] = 1;
		equipo1[10][5] = "Málaga"; equipo2[10][5] = "Betis"; goles1[10][5] = 0; goles2[10][5] = 1;
		equipo1[10][6] = "Athletic"; equipo2[10][6] = "Espanyol"; goles1[10][6] = 2; goles2[10][6] = 1;
		equipo1[10][7] = "Barcelona"; equipo2[10][7] = "Villarreal"; goles1[10][7] = 3; goles2[10][7] = 0;
		equipo1[10][8] = "Atlético"; equipo2[10][8] = "Sporting"; goles1[10][8] = 1; goles2[10][8] = 0;
		equipo1[10][9] = "Sevilla"; equipo2[10][9] = "R.Madrid"; goles1[10][9] = 3; goles2[10][9] = 2;
		   
		equipo1[11][0] = "Getafe"; equipo2[11][0] = "Rayo"; goles1[11][0] = 1; goles2[11][0] = 1;
		equipo1[11][1] = "R.Sociedad"; equipo2[11][1] = "Sevilla"; goles1[11][1] = 2; goles2[11][1] = 0;
		equipo1[11][2] = "R.Madrid"; equipo2[11][2] = "Barcelona"; goles1[11][2] = 0; goles2[11][2] = 4;
		equipo1[11][3] = "Espanyol"; equipo2[11][3] = "Málaga"; goles1[11][3] = 2; goles2[11][3] = 0;
		equipo1[11][4] = "Valencia"; equipo2[11][4] = "LasPalmas"; goles1[11][4] = 1; goles2[11][4] = 1;
		equipo1[11][5] = "Deportivo"; equipo2[11][5] = "Celta"; goles1[11][5] = 2; goles2[11][5] = 0;
		equipo1[11][6] = "Sporting"; equipo2[11][6] = "Levante"; goles1[11][6] = 0; goles2[11][6] = 3;
		equipo1[11][7] = "Villarreal"; equipo2[11][7] = "Eibar"; goles1[11][7] = 1; goles2[11][7] = 1;
		equipo1[11][8] = "Granada"; equipo2[11][8] = "Athletic"; goles1[11][8] = 2; goles2[11][8] = 0;
		equipo1[11][9] = "Betis"; equipo2[11][9] = "Atlético"; goles1[11][9] = 0; goles2[11][9] = 1;
		   
		equipo1[12][0] = "Levante"; equipo2[12][0] = "Betis"; goles1[12][0] = 0; goles2[12][0] = 1;
		equipo1[12][1] = "Barcelona"; equipo2[12][1] = "R.Sociedad"; goles1[12][1] = 4; goles2[12][1] = 0;
		equipo1[12][2] = "Atlético"; equipo2[12][2] = "Espanyol"; goles1[12][2] = 1; goles2[12][2] = 0;
		equipo1[12][3] = "Málaga"; equipo2[12][3] = "Granada"; goles1[12][3] = 2; goles2[12][3] = 2;
		equipo1[12][4] = "Celta"; equipo2[12][4] = "Sporting"; goles1[12][4] = 2; goles2[12][4] = 1;
		equipo1[12][5] = "LasPalmas"; equipo2[12][5] = "Deportivo"; goles1[12][5] = 0; goles2[12][5] = 2;
		equipo1[12][6] = "Getafe"; equipo2[12][6] = "Villarreal"; goles1[12][6] = 2; goles2[12][6] = 0;
		equipo1[12][7] = "Eibar"; equipo2[12][7] = "R.Madrid"; goles1[12][7] = 0; goles2[12][7] = 2;
		equipo1[12][8] = "Rayo"; equipo2[12][8] = "Athletic"; goles1[12][8] = 0; goles2[12][8] = 3;
		equipo1[12][9] = "Sevilla"; equipo2[12][9] = "Valencia"; goles1[12][9] = 1; goles2[12][9] = 0;
		   
		equipo1[13][0] = "R.Madrid"; equipo2[13][0] = "Getafe"; goles1[13][0] = 4; goles2[13][0] = 1;
		equipo1[13][1] = "Granada"; equipo2[13][1] = "Atlético"; goles1[13][1] = 0; goles2[13][1] = 2;
		equipo1[13][2] = "Valencia"; equipo2[13][2] = "Barcelona"; goles1[13][2] = 1; goles2[13][2] = 1;
		equipo1[13][3] = "Deportivo"; equipo2[13][3] = "Sevilla"; goles1[13][3] = 1; goles2[13][3] = 1;
		equipo1[13][4] = "Betis"; equipo2[13][4] = "Celta"; goles1[13][4] = 1; goles2[13][4] = 1;
		equipo1[13][5] = "R.Sociedad"; equipo2[13][5] = "Eibar"; goles1[13][5] = 2; goles2[13][5] = 1;
		equipo1[13][6] = "Villarreal"; equipo2[13][6] = "Rayo"; goles1[13][6] = 2; goles2[13][6] = 1;
		equipo1[13][7] = "Sporting"; equipo2[13][7] = "LasPalmas"; goles1[13][7] = 3; goles2[13][7] = 1;
		equipo1[13][8] = "Athletic"; equipo2[13][8] = "Málaga"; goles1[13][8] = 0; goles2[13][8] = 0;
		equipo1[13][9] = "Espanyol"; equipo2[13][9] = "Levante"; goles1[13][9] = 1; goles2[13][9] = 1;
		   
		equipo1[14][0] = "Atlético"; equipo2[14][0] = "Athletic"; goles1[14][0] = 2; goles2[14][0] = 1;
		equipo1[14][1] = "Villarreal"; equipo2[14][1] = "R.Madrid"; goles1[14][1] = 1; goles2[14][1] = 0;
		equipo1[14][2] = "Eibar"; equipo2[14][2] = "Valencia"; goles1[14][2] = 1; goles2[14][2] = 1;
		equipo1[14][3] = "Rayo"; equipo2[14][3] = "Málaga"; goles1[14][3] = 1; goles2[14][3] = 2;
		equipo1[14][4] = "LasPalmas"; equipo2[14][4] = "Betis"; goles1[14][4] = 1; goles2[14][4] = 0;
		equipo1[14][5] = "Sevilla"; equipo2[14][5] = "Sporting"; goles1[14][5] = 2; goles2[14][5] = 0;
		equipo1[14][6] = "Celta"; equipo2[14][6] = "Espanyol"; goles1[14][6] = 1; goles2[14][6] = 0;
		equipo1[14][7] = "Levante"; equipo2[14][7] = "Granada"; goles1[14][7] = 1; goles2[14][7] = 2;
		equipo1[14][8] = "Barcelona"; equipo2[14][8] = "Deportivo"; goles1[14][8] = 2; goles2[14][8] = 2;
		equipo1[14][9] = "Getafe"; equipo2[14][9] = "R.Sociedad"; goles1[14][9] = 1; goles2[14][9] = 1;
		   
		equipo1[15][0] = "Valencia"; equipo2[15][0] = "Getafe"; goles1[15][0] = 2; goles2[15][0] = 2;
		equipo1[15][1] = "Espanyol"; equipo2[15][1] = "LasPalmas"; goles1[15][1] = 1; goles2[15][1] = 0;
		equipo1[15][2] = "Betis"; equipo2[15][2] = "Sevilla"; goles1[15][2] = 0; goles2[15][2] = 0;
		equipo1[15][3] = "Deportivo"; equipo2[15][3] = "Eibar"; goles1[15][3] = 2; goles2[15][3] = 0;
		equipo1[15][4] = "R.Madrid"; equipo2[15][4] = "Rayo"; goles1[15][4] = 10; goles2[15][4] = 2;
		equipo1[15][5] = "R.Sociedad"; equipo2[15][5] = "Villarreal"; goles1[15][5] = 0; goles2[15][5] = 2;
		equipo1[15][6] = "Athletic"; equipo2[15][6] = "Levante"; goles1[15][6] = 2; goles2[15][6] = 0;
		equipo1[15][7] = "Granada"; equipo2[15][7] = "Celta"; goles1[15][7] = 0; goles2[15][7] = 2;
		equipo1[15][8] = "Málaga"; equipo2[15][8] = "Atlético"; goles1[15][8] = 1; goles2[15][8] = 0;
		equipo1[15][9] = "Sporting"; equipo2[15][9] = "Barcelona"; goles1[15][9] = 1; goles2[15][9] = 3;
		   
		equipo1[16][0] = "R.Madrid"; equipo2[16][0] = "R.Sociedad"; goles1[16][0] = 3; goles2[16][0] = 1;
		equipo1[16][1] = "Levante"; equipo2[16][1] = "Málaga"; goles1[16][1] = 0; goles2[16][1] = 1;
		equipo1[16][2] = "Rayo"; equipo2[16][2] = "Atlético"; goles1[16][2] = 0; goles2[16][2] = 2;
		equipo1[16][3] = "Sevilla"; equipo2[16][3] = "Espanyol"; goles1[16][3] = 2; goles2[16][3] = 0;
		equipo1[16][4] = "Eibar"; equipo2[16][4] = "Sporting"; goles1[16][4] = 2; goles2[16][4] = 0;
		equipo1[16][5] = "Barcelona"; equipo2[16][5] = "Betis"; goles1[16][5] = 4; goles2[16][5] = 0;
		equipo1[16][6] = "Getafe"; equipo2[16][6] = "Deportivo"; goles1[16][6] = 0; goles2[16][6] = 0;
		equipo1[16][7] = "Celta"; equipo2[16][7] = "Athletic"; goles1[16][7] = 0; goles2[16][7] = 1;
		equipo1[16][8] = "LasPalmas"; equipo2[16][8] = "Granada"; goles1[16][8] = 4; goles2[16][8] = 1;
		equipo1[16][9] = "Villarreal"; equipo2[16][9] = "Valencia"; goles1[16][9] = 1; goles2[16][9] = 0;
		   
		equipo1[17][0] = "Sporting"; equipo2[17][0] = "Getafe"; goles1[17][0] = 1; goles2[17][0] = 2;
		equipo1[17][1] = "Valencia"; equipo2[17][1] = "R.Madrid"; goles1[17][1] = 2; goles2[17][1] = 2;
		equipo1[17][2] = "Deportivo"; equipo2[17][2] = "Villarreal"; goles1[17][2] = 1; goles2[17][2] = 2;
		equipo1[17][3] = "Athletic"; equipo2[17][3] = "LasPalmas"; goles1[17][3] = 2; goles2[17][3] = 2;
		equipo1[17][4] = "Granada"; equipo2[17][4] = "Sevilla"; goles1[17][4] = 2; goles2[17][4] = 1;
		equipo1[17][5] = "Betis"; equipo2[17][5] = "Eibar"; goles1[17][5] = 0; goles2[17][5] = 4;
		equipo1[17][6] = "Rayo"; equipo2[17][6] = "R.Sociedad"; goles1[17][6] = 2; goles2[17][6] = 2;
		equipo1[17][7] = "Málaga"; equipo2[17][7] = "Celta"; goles1[17][7] = 2; goles2[17][7] = 0;
		equipo1[17][8] = "Atlético"; equipo2[17][8] = "Levante"; goles1[17][8] = 1; goles2[17][8] = 0;
		equipo1[17][9] = "Espanyol"; equipo2[17][9] = "Barcelona"; goles1[17][9] = 0; goles2[17][9] = 0;
		   
		equipo1[18][0] = "Barcelona"; equipo2[18][0] = "Granada"; goles1[18][0] = 4; goles2[18][0] = 0;
		equipo1[18][1] = "Sevilla"; equipo2[18][1] = "Athletic"; goles1[18][1] = 2; goles2[18][1] = 0;
		equipo1[18][2] = "Getafe"; equipo2[18][2] = "Betis"; goles1[18][2] = 1; goles2[18][2] = 0;
		equipo1[18][3] = "R.Madrid"; equipo2[18][3] = "Deportivo"; goles1[18][3] = 5; goles2[18][3] = 0;
		equipo1[18][4] = "Levante"; equipo2[18][4] = "Rayo"; goles1[18][4] = 2; goles2[18][4] = 1;
		equipo1[18][5] = "Villarreal"; equipo2[18][5] = "Sporting"; goles1[18][5] = 2; goles2[18][5] = 0;
		equipo1[18][6] = "R.Sociedad"; equipo2[18][6] = "Valencia"; goles1[18][6] = 2; goles2[18][6] = 0;
		equipo1[18][7] = "Eibar"; equipo2[18][7] = "Espanyol"; goles1[18][7] = 2; goles2[18][7] = 1;
		equipo1[18][8] = "LasPalmas"; equipo2[18][8] = "Málaga"; goles1[18][8] = 1; goles2[18][8] = 1;
		equipo1[18][9] = "Celta"; equipo2[18][9] = "Atlético"; goles1[18][9] = 0; goles2[18][9] = 2;
		   
		equipo1[19][0] = "Eibar"; equipo2[19][0] = "Granada"; goles1[19][0] = 5; goles2[19][0] = 1;
		equipo1[19][1] = "Barcelona"; equipo2[19][1] = "Athletic"; goles1[19][1] = 6; goles2[19][1] = 0;
		equipo1[19][2] = "Getafe"; equipo2[19][2] = "Espanyol"; goles1[19][2] = 3; goles2[19][2] = 1;
		equipo1[19][3] = "LasPalmas"; equipo2[19][3] = "Atlético"; goles1[19][3] = 0; goles2[19][3] = 3;
		equipo1[19][4] = "R.Madrid"; equipo2[19][4] = "Sporting"; goles1[19][4] = 5; goles2[19][4] = 1;
		equipo1[19][5] = "Valencia"; equipo2[19][5] = "Rayo"; goles1[19][5] = 2; goles2[19][5] = 2;
		equipo1[19][6] = "R.Sociedad"; equipo2[19][6] = "Deportivo"; goles1[19][6] = 1; goles2[19][6] = 1;
		equipo1[19][7] = "Villarreal"; equipo2[19][7] = "Betis"; goles1[19][7] = 0; goles2[19][7] = 0;
		equipo1[19][8] = "Celta"; equipo2[19][8] = "Levante"; goles1[19][8] = 4; goles2[19][8] = 3;
		equipo1[19][9] = "Sevilla"; equipo2[19][9] = "Málaga"; goles1[19][9] = 2; goles2[19][9] = 1;
		   
		equipo1[20][0] = "Levante"; equipo2[20][0] = "LasPalmas"; goles1[20][0] = 3; goles2[20][0] = 2;
		equipo1[20][1] = "Betis"; equipo2[20][1] = "R.Madrid"; goles1[20][1] = 1; goles2[20][1] = 1;
		equipo1[20][2] = "Deportivo"; equipo2[20][2] = "Valencia"; goles1[20][2] = 1; goles2[20][2] = 1;
		equipo1[20][3] = "Sporting"; equipo2[20][3] = "R.Sociedad"; goles1[20][3] = 5; goles2[20][3] = 1;
		equipo1[20][4] = "Málaga"; equipo2[20][4] = "Barcelona"; goles1[20][4] = 1; goles2[20][4] = 2;
		equipo1[20][5] = "Espanyol"; equipo2[20][5] = "Villarreal"; goles1[20][5] = 2; goles2[20][5] = 2;
		equipo1[20][6] = "Granada"; equipo2[20][6] = "Getafe"; goles1[20][6] = 3; goles2[20][6] = 2;
		equipo1[20][7] = "Rayo"; equipo2[20][7] = "Celta"; goles1[20][7] = 3; goles2[20][7] = 0;
		equipo1[20][8] = "Athletic"; equipo2[20][8] = "Eibar"; goles1[20][8] = 5; goles2[20][8] = 2;
		equipo1[20][9] = "Atlético"; equipo2[20][9] = "Sevilla"; goles1[20][9] = 0; goles2[20][9] = 0;
		   
		equipo1[21][0] = "Deportivo"; equipo2[21][0] = "Rayo"; goles1[21][0] = 2; goles2[21][0] = 2;
		equipo1[21][1] = "R.Madrid"; equipo2[21][1] = "Espanyol"; goles1[21][1] = 6; goles2[21][1] = 0;
		equipo1[21][2] = "LasPalmas"; equipo2[21][2] = "Celta"; goles1[21][2] = 2; goles2[21][2] = 1;
		equipo1[21][3] = "Valencia"; equipo2[21][3] = "Sporting"; goles1[21][3] = 0; goles2[21][3] = 1;
		equipo1[21][4] = "Sevilla"; equipo2[21][4] = "Levante"; goles1[21][4] = 3; goles2[21][4] = 1;
		equipo1[21][5] = "R.Sociedad"; equipo2[21][5] = "Betis"; goles1[21][5] = 2; goles2[21][5] = 1;
		equipo1[21][6] = "Villarreal"; equipo2[21][6] = "Granada"; goles1[21][6] = 1; goles2[21][6] = 0;
		equipo1[21][7] = "Eibar"; equipo2[21][7] = "Málaga"; goles1[21][7] = 1; goles2[21][7] = 2;
		equipo1[21][8] = "Getafe"; equipo2[21][8] = "Athletic"; goles1[21][8] = 0; goles2[21][8] = 1;
		equipo1[21][9] = "Barcelona"; equipo2[21][9] = "Atlético"; goles1[21][9] = 2; goles2[21][9] = 1;
		   
		equipo1[22][0] = "Espanyol"; equipo2[22][0] = "R.Sociedad"; goles1[22][0] = 0; goles2[22][0] = 5;
		equipo1[22][1] = "Granada"; equipo2[22][1] = "R.Madrid"; goles1[22][1] = 1; goles2[22][1] = 2;
		equipo1[22][2] = "Celta"; equipo2[22][2] = "Sevilla"; goles1[22][2] = 1; goles2[22][2] = 1;
		equipo1[22][3] = "Betis"; equipo2[22][3] = "Valencia"; goles1[22][3] = 1; goles2[22][3] = 0;
		equipo1[22][4] = "Levante"; equipo2[22][4] = "Barcelona"; goles1[22][4] = 0; goles2[22][4] = 2;
		equipo1[22][5] = "Atlético"; equipo2[22][5] = "Eibar"; goles1[22][5] = 3; goles2[22][5] = 1;
		equipo1[22][6] = "Rayo"; equipo2[22][6] = "LasPalmas"; goles1[22][6] = 2; goles2[22][6] = 0;
		equipo1[22][7] = "Málaga"; equipo2[22][7] = "Getafe"; goles1[22][7] = 3; goles2[22][7] = 0;
		equipo1[22][8] = "Athletic"; equipo2[22][8] = "Villarreal"; goles1[22][8] = 0; goles2[22][8] = 0;
		equipo1[22][9] = "Sporting"; equipo2[22][9] = "Deportivo"; goles1[22][9] = 1; goles2[22][9] = 1;
		   
		equipo1[23][0] = "R.Sociedad"; equipo2[23][0] = "Granada"; goles1[23][0] = 3; goles2[23][0] = 0;
		equipo1[23][1] = "Sevilla"; equipo2[23][1] = "LasPalmas"; goles1[23][1] = 2; goles2[23][1] = 0;
		equipo1[23][2] = "Getafe"; equipo2[23][2] = "Atlético"; goles1[23][2] = 0; goles2[23][2] = 1;
		equipo1[23][3] = "Eibar"; equipo2[23][3] = "Levante"; goles1[23][3] = 2; goles2[23][3] = 0;
		equipo1[23][4] = "Barcelona"; equipo2[23][4] = "Celta"; goles1[23][4] = 6; goles2[23][4] = 1;
		equipo1[23][5] = "Deportivo"; equipo2[23][5] = "Betis"; goles1[23][5] = 2; goles2[23][5] = 2;
		equipo1[23][6] = "Valencia"; equipo2[23][6] = "Espanyol"; goles1[23][6] = 2; goles2[23][6] = 1;
		equipo1[23][7] = "Villarreal"; equipo2[23][7] = "Málaga"; goles1[23][7] = 1; goles2[23][7] = 0;
		equipo1[23][8] = "R.Madrid"; equipo2[23][8] = "Athletic"; goles1[23][8] = 4; goles2[23][8] = 2;
		equipo1[23][9] = "Sporting"; equipo2[23][9] = "Rayo"; goles1[23][9] = 2; goles2[23][9] = 2;
		   
		equipo1[24][0] = "Atlético"; equipo2[24][0] = "Villarreal"; goles1[24][0] = 0; goles2[24][0] = 0;
		equipo1[24][1] = "Athletic"; equipo2[24][1] = "R.Sociedad"; goles1[24][1] = 0; goles2[24][1] = 1;
		equipo1[24][2] = "Granada"; equipo2[24][2] = "Valencia"; goles1[24][2] = 1; goles2[24][2] = 2;
		equipo1[24][3] = "Málaga"; equipo2[24][3] = "R.Madrid"; goles1[24][3] = 1; goles2[24][3] = 1;
		equipo1[24][4] = "Rayo"; equipo2[24][4] = "Sevilla"; goles1[24][4] = 2; goles2[24][4] = 2;
		equipo1[24][5] = "Celta"; equipo2[24][5] = "Eibar"; goles1[24][5] = 3; goles2[24][5] = 2;
		equipo1[24][6] = "Betis"; equipo2[24][6] = "Sporting"; goles1[24][6] = 1; goles2[24][6] = 1;
		equipo1[24][7] = "Espanyol"; equipo2[24][7] = "Deportivo"; goles1[24][7] = 1; goles2[24][7] = 0;
		equipo1[24][8] = "LasPalmas"; equipo2[24][8] = "Barcelona"; goles1[24][8] = 1; goles2[24][8] = 2;
		equipo1[24][9] = "Levante"; equipo2[24][9] = "Getafe"; goles1[24][9] = 3; goles2[24][9] = 0;
	
	}
		
	//metodos de clase --> usar siempre STATIC
	public static String getEquipo1( int numJornada , int numPartido )
	{
			// nombre de la tabla fila y columna
		return equipo1[numJornada][numPartido];
	}
	
	public static String getEquipo2( int numJornada , int numPartido )
	{
		return equipo2[numJornada][numPartido];
	}
	
	public static int getGoles1( int numJornada , int numPartido )
	{
		return goles1[numJornada][numPartido];
	}
	
	public static int getGoles2( int numJornada , int numPartido )
	{
		return goles2[numJornada][numPartido];
	}
	
	public static int getJornadas()
	{
		int jornadas = 0;
		while ( equipo1[ jornadas ][ 0 ] != null )
		{
			jornadas++;
		}		
		return jornadas;
	}
	
	public static void main(String args[]) {
		int jornadasJugadas = getJornadas();

		for (int jornada = 0; jornada < jornadasJugadas; jornada++) {
			System.out.println("Jornada " + jornada);
			for (int partido = 0; partido < 10; partido++) {
				String e1 = getEquipo1(jornada, partido);
				String e2 = getEquipo2(jornada, partido);
				int g1 = getGoles1(jornada, partido);
				int g2 = getGoles2(jornada, partido);

				System.out.println(e1 + "-" + e2 + " " + g1 + "-" + g2);
			}
			System.out.println("---------------------");
		}
	}
}
